<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'bootstrap' => ['parser'],
    'modules' => [
        'parser' => [
            'class' => 'common\modules\parser\ParserModule',
        ],
    ],
];
