<?php

namespace common\modules\parser;

use yii;
use yii\base\BootstrapInterface;
use yii\base\Module as BaseModule;

class ParserModule extends BaseModule implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
//    public $controllerNamespace = 'common\modules\parser\controllers';
//    public $defaultRoute = 'micex/run';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // load config
        Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'common\modules\parser\commands';
        }
    }
}
