<?php

namespace common\modules\parser\commands;

use \yii\console\Controller;
use common\modules\parser\ParserModule;
use common\modules\parser\models;
use GuzzleHttp\Client;

/**
 * Default controller for the `parser` module
 */
class MicexController extends Controller
{
    const HTTP_OK = 200;

    public function actionRun()
    {
        $_module = ParserModule::getInstance();

        // получить данные
        $client = new Client();
        $response = $client->request('GET', $_module->params['micex.url']);
        $body = json_decode($response->getBody(), true);

        // сохранить модель
        if ($response->getStatusCode() == self::HTTP_OK) {
            // название атрибутов
            $_columns = array_map('strtolower', $body['marketdata']['columns']);
            foreach ($body['marketdata']['data'] as $_item) {
                // склеиваем атрибуты - значения
                $_attributes = array_combine($_columns, $_item);
                //сохраняем для эмитентов из конфига
                if (in_array($_attributes['secid'], $_module->params['micex.issuers'])) {
                    $model = new models\MicexParser();
                    $model->attributes = array_combine($_columns, $_item);
                    $model->save();
                }
            }
        }else{
            return Controller::EXIT_CODE_ERROR;
        }
        return Controller::EXIT_CODE_NORMAL;
    }
}
