<?php

return [
    'params' => [
        'micex.url' => 'http://www.micex.ru/iss/engines/stock/markets/shares/boardgroups/57/securities.json',
        'micex.issuers' => ['ABRD', 'AESL'],
    ],
];