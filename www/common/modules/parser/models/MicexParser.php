<?php

namespace common\modules\parser\models;

use Yii;

/**
 * This is the model class for table "micex".
 *
 * @property integer $id
 * @property string $timestamp
 * @property string $secid
 * @property double $last
 */
class MicexParser extends \yii\db\ActiveRecord
{
    const SCENARIO_PARSE = 'parser';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'micex';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['secid', 'last'], 'safe'],
        ];
    }

}
