<?php

use yii\db\Migration;

/**
 * Handles adding email_token to table `user`.
 */
class m161206_182245_add_email_token_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'email_token', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'email_token');
    }
}
