<?php

use yii\db\Migration;

/**
 * Handles the creation of table `micex`.
 */
class m161208_141531_create_micex_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('micex', [
            'id' => $this->primaryKey(),
            'timestamp' => $this->timestamp(),
            'secid' => $this->char(36)->notNull(),
            'last' => $this->double(),
        ]);

        $this->execute('ALTER TABLE `micex` MODIFY timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('micex');
    }
}
