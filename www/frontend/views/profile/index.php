<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
//            'email',
        ],
    ]) ?>
    <p>
        <?= Html::a('UPDATE', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>
</div>